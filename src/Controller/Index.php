<?php

namespace App\Controller;

use Vrame\Controller;

class Index extends Controller {

  public function index() {
    $this->renderText("Welcome to Vrame!");
  }

}
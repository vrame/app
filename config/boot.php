<?php

// load routes config
include_once 'routes.php';

function __($string) {
    return Vrame\Locale::translate($string);
}

function currency_format($value, $decimals=2, $unit="$") {
  return $unit.number_format($value, $decimals, ",", ".");
}

?>
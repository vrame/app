<?php

Vrame\Router::load(array(
  '^/games/(\d+)/?.*'   => '/games/index/$1',
  '^/game/(\d+)/?.*'    => '/game/index/$1',
  '^/betting/.+/(\d+)$' => '/betting/show/$1',
  '^/data/?$'           => '/index/data',
  '^/terms/?$'          => '/pages/show/terms',
  '^/deposit/?$'        => '/pages/show/deposit',
  '^/store/(\d+)$'      => '/store/index/$1',
  '^/multiplayer/coinflip/(\d+)$' => '/multiplayer/coinflip_game/$1'
));

?>